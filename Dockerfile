#  This is used to build the container image with docker.
# creates a layer from the openjdk:17-alpine Docker image.
FROM openjdk:17-alpine

ADD /target/consoleapp.jar consoleapp.jar 

# java -jar /app/consoleapp.jar
CMD ["java", "-jar", "consoleapp.jar"]

# Make port 8080 available outside this container
EXPOSE 8080