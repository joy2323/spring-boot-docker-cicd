package com.usertest.springbootdockergitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDockerGitlabApplication {
	@GetMapping("/welcome")
	public String getMessage() {
		return "Welcome to Springboot Docker Amazon Linux 2 Gitlab Pipeline Tutorial...!!!";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerGitlabApplication.class, args);
	}

}
