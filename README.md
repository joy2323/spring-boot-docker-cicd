# Spring Boot API deployment on AWS EC2 with GitLab CI/CD and Docker
- create a simple Rest Spring Boot application using Spring Initializr. => https://start.spring.io
- create a new project on Gitlab, commit my code locally, and push it to the remote repository using the project URL Gitlab generated.
 
 ### Launch an Amazon Linux 2 instance
 - Select the Amazon Linux AMI free tier instance type - t2.micro
 - configured Security group to allow app be run on port 8080, add an inbound rule with type Custom TCP on port 8080 to Anywhere.
 - Select/Generate a key pair for ssh purpose.
 - Name the EC2 Instance - (spring-boot-app)
 - created a Security Group — launch-wizard-1
 - using the default VPC.

 ### Dockerize Spring Boot Application
 - Define Dockerfile
 - Generate Docker Image and test locally

 ### Launch Application in AWS EC2
 - on mdy terminal, navigate to the key pair and modify the permissions.
 `chmod 600 <key-pair.pem>`
 - ssh into the instance with the image username => ec2-user
 `ssh -i /path/keypair.pem ec2-user@<Instance-Public-IP>`
 - Install the updates to the instance. 
    > sudo yum update -y
 - install tomcat to be sure and user can access the IP address via their browser. - This is not compulsory but had to do it to be sure that port 8080 is open and accessible
 - Install and Start docker compose
    > sudo yum search docker
    > sudo yum info docker
    > sudo usermod -a -G docker ec2-user
    > id ec2-user
    > sudo yum install python3-pip
    > sudo pip3 install docker-compose
    > sudo systemctl enable docker.service
    > sudo systemctl start docker.service
    > sudo systemctl status docker.service

 ### Set up Runner - Run GitLab Runner in a container
 - configure GitLab Runner to support Docker commands. docker-compose.yml file contains all the instructions to run the gitlab-runner container.
 - use docker compose up -d to run the runner container. create config/config.toml file
 - Register the new runner. The GitLab Runner container doesn’t pick up any jobs until it’s registered.
 - Go to Project Settings > CI / CD > Runner section and get a token.
 - Register GitLab Runner from the command line with command below:

   > docker compose run --rm gitlab-runner register -n \
   > --url https://gitlab.com/ \
   > --registration-token <TOKEN>\
   > --executor docker \
   > --description "Demo test my docker runner" \
   > --docker-image "docker:stable" \
   > --docker-privileged \
   > --docker-volumes /var/run/docker.sock:/var/run/docker.sock

   - This command registers a new runner to use the docker:stable image. To start the build and service containers, it uses the privileged mode
   - When the runner registration is successful, the config/config.toml file is updated with this content:
      concurrent = 1
      check_interval = 0
      shutdown_timeout = 0

      [session_server]
      session_timeout = 1800

      [[runners]]
      name = "Demo test my docker runner"
      url = "https://gitlab.com/"
      id = 28418383
      token = "uBchgLoRBbr3g42XQNKx"
      token_obtained_at = 2023-10-12T14:54:11Z
      token_expires_at = 0001-01-01T00:00:00Z
      executor = "docker"
      [runners.cache]
         MaxUploadedArchiveSize = 0
      [runners.docker]
         tls_verify = false
         image = "docker:stable"
         privileged = true
         disable_entrypoint_overwrite = false
         oom_kill_disable = false
         disable_cache = false
         volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
         shm_size = 0
  - Back on GitLab, you will see the registered runner in Settings > CI / CD > Runner section - see image in the repository
  ### .gitlab-ci.yml script
 -On gitlab, set up the CICD variables for running jobs - DEPLOY_SERVER_IP, SSH_PRIVATE_KEY 
 -  create a .gitlab-ci.yml file at the project root directory that will instruct a GitLab runner how to build and deploy our app. The pipeline consists of three stages: build, test and deploy.

 APP URL: http://ec2-35-177-79-221.eu-west-2.compute.amazonaws.com:8080/welcome
    



